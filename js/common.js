app.service('SrvUser',['$http',function($http){
    var service = {
        Login:function(objUser) {
            return $http.post('http://localhost/ecommerce_backend/public/api/signin',objUser).catch(function(response){

            });

        },
        Register:function(objUser){
            return $http.post('http://localhost/ecommerce_backend/public/api/signup',objUser).catch(function(response){

            });
        }
    }

    return service;
}])

app.service('SrvProductCategory',['$http',function($http){
    var service = {
        List:function() {
            return $http.get('http://localhost/ecommerce_backend/public/api/listCategory').catch(function(response){

            });

        },
        GetProductAttachedtoCategory:function(category_id) {
            return $http.get('http://localhost/ecommerce_backend/public/api/listProductAttachedtoCategory/'+category_id).catch(function(response){

            });

        },
    }

    return service;
}])