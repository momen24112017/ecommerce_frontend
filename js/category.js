var app = angular.module('myApp',['toaster', 'ngAnimate']);

app.controller('myCtrl',function($scope,SrvUser,SrvProductCategory,toaster) { 



    $scope.arrProductCategory = [];

    $scope.init = function(){
        $scope.listProductAttachedtoCategory(localStorage.getItem("category_id"))
    }
    $scope.listProductAttachedtoCategory = function(category_id){
        SrvProductCategory.GetProductAttachedtoCategory(category_id).then(function(response){
            if(response.data.status.code==200){
                $scope.arrProductCategory = response.data.data.data;
            }else{

            }
        })
    }
    $scope.init();
})