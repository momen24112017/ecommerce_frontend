var app = angular.module('myApp',['toaster', 'ngAnimate']);

app.controller('myCtrl',function($scope,SrvUser,SrvProductCategory,toaster) { 

$scope.objLoginUser = {"email":"","password":""};
$scope.objRegisterUser = {"name":"","email":"","password":""};
$scope.arrCategory = [];

$scope.init = function(){
    $scope.listCategory();
}
$scope.login = function(){
    SrvUser.Login($scope.objLoginUser).then(function(response){
        if(response.data.status.code == 200){
            // toaster.pop({
            //     type: 'Success',
            //     title: 'login',
            //     body: 'logged it successfully',
            //     timeout: 3000
            // });
            alert('hello');
            localStorage.setItem("user",response.data.data);
            window.location.href="category.html";
        }else{
            alert("bad");
        }
    })
}
$scope.register = function(){
    SrvUser.Register($scope.objRegisterUser).then(function(response){
        if(response.data.status.code == 200){
            alert('hello');
            localStorage.setItem("user",response.data.data);
            window.location.href="category.html";
        }else{
            alert("bad");
        }
    })
    
}
$scope.listCategory = function(){
    SrvProductCategory.List().then(function(response){
        if(response.data.status.code == 200){
            $scope.arrCategory = response.data.data;
        }else{

        }
    })
}

$scope.getProductAttachedtoCategory = function(category_id){
    localStorage.setItem("category_id",category_id);
    window.location.href="category.html";
}

$scope.init();
})